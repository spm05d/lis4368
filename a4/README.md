> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

lis4368

## Samuel Meador

### Assignment 4 Requirements:

1. Clone Updated Student_Files
2. Compile Class and Servlet Files
3. Add Server-side Validation   

#### README.md file should include the following items:

* Screenshot of blank Assignment 4 form
* Screenshot of Completed and Processed Assignment 4 Form

#### Assignment Screenshots and Links:

*Screenshot Blank A4 Form*:

![A4 Blank Form](img/blankA4.png "Blank Assignment 4 Form")

*Screenshot Completed/Processed A4 Form*:

![A4 Completed/Processed Form](img/completedA4.png "Completed Assignment 4 Form")