> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

lis4368 Advance Web App Development

Sam Meador

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    1. Install JDK
    2. Install Tomcat
    3. Provide Screenshots of installations
    4. Create Bitbucket Repo
    5. Complete Bitbucket Tutorials
    6. Provide Git command Descriptions
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
    1. Setting Environment Variables
    2. CRUD Application using MySQL
    3. Java Server Pages and Servlets

3. [A3 README.md](a3/README.md "My A3 README.md file")
    1. Entity Relationship Diagram
    2. Include data (at least 10 records in each table)
    3. Provide Bitbucket Read-only access to repo (language SQL) *must* include README.md usuing Markdown Syntax, and includes links to *all*      of the folling files (from README.md):
        - docs folder: a3.mwb and a3.sql
        - img folder: a3.png (export a3.mwb as a3.png)
        - README.md (*MUST* display a3.png ERD) 

4. [A4 README.md](a4/README.md "My A4 README.md file")
    1. Clone Updated Student_Files
    2. Compile Class and Servlet Files
    3. Add Server-side Validation  

5. [A5 README.md](a5/README.md "My A5 README.md file")
    1. Compile Class and Servlet Files
    2. Include server-side validation from A4
    3. Display screenshots of pre-, post-valid user entry form entry, as well as MySQL customer table

6. [P1 README.md](p1/README.md "My P1 README.md file")
    1. Bootstrap Client-side Validation
    2. Regular Expressions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    1. Compile class and servlet files
    2. Include server-side validation from Assignment 4
    3. README displays screenshots of pre-, post-valid user form entry, as well as MySQL customer table entry