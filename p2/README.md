> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

lis4368

## Samuel Meador

### Project 2 Requirements:

    1. Compile class and servlet files
    2. Include server-side validation from Assignment 4
    3. README displays screenshots of pre-, post-valid user form entry, as well as MySQL customer table entry

#### README.md file should include the following items:

* Valid User Form Entry
* Passed Validation
* Display Data
* Modify Form
* Modified Data
* Delete Warning
* Associated Database Changes 

#### Assignment Screenshots and Links:

![Valid User Entry](img/data_input.png "Valid User Entry")

![Passed Validation](img/accepted_customer.png "Passed Validation")

![Display Data](img/modify.png "Display Data")

![Modify Form](img/edit_data.png "Modify Form")

![Modified Data](img/modified_data.png "Modified Data")

![Delete Warning](img/ask_delete.png "Delete Warning")

![Associated Database Changes (Before)](img/mysql_before.png "Associated Database Changes")

![Associated Database Changes (After)](img/mysql_after.png "Associated Database Changes")

![Associated Database Changes (Update/Delete)](img/mysql_update.png "Associated Database Changes")