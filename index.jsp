<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Sam Meador">
	<link rel="icon" href="favicon.ico">

	<title>Mr. Meador's Online Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 90px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}

.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}

.carousel
{
  margin: 20px 0px 20px 0px;
}

.bs-example
{
  margin: 20px;
}

#slide1 {
	color: #000;
}

#slide2 {
	color: #000;
}

#slide3 {
	color: #000;
}
#slide4 {
	color: #000;	
}
</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="6000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>   
	   <!-- Carousel items -->
        <div class="carousel-inner">

				 <div class="active item" style="background: url(img/tomcat_ss.png) no-repeat top; ">
					 <div class="container">
						 <div class="carousel-caption">
								<h3 id="slide1">Assignment 1</h3>
							 <p class="lead" id="slide1">JDK and Tomcat installations and Bootstrap framework</p>
							 <a class="btn btn-large btn-primary" href="a1/index.jsp">View</a>
						 </div>
					 </div>
				 </div>					

            <div class="item" style="background: url(img/mysql.png) no-repeat top;">
                <div class="container">
                	<div class="carousel-caption">
						<h3 id="slide2">Assignment 2</h3>
						<p class="lead" id="slide2">MySQL and Data-driven Website</p>
						<a class="btn btn-large btn-primary" href="a2/index.jsp">View</a>
<!--					<img src="img/slide2.png" alt="Slide 2">	-->
					</div>
				</div>	
            </div>

            <div class="item" style="background: url(img/database.png) no-repeat top;">
                <h2></h2>
                <div class="carousel-caption">
					<h3 id="slide3">Assignment 3</h3>
					<p class="lead" id="slide3">Begin Web Application Development - Create Database</p>
					<a class="btn btn-large btn-primary" href="a3/index.jsp">View</a>
<!--				<img src="img/slide3.png" class="img-responsive" alt="Slide 3">	-->
                </div>
			</div>
			
			<div class="item" style="background: url(img/p1.png) no-repeat top;">
                <h2></h2>
                <div class="carousel-caption">
					<h3 id="slide3">Project 1</h3>
					<p class="lead" id="slide4">Basic client-side validation</p>
					<a class="btn btn-large btn-primary" href="p1/index.jsp">View</a>
<!--				<img src="img/slide4.png" class="img-responsive" alt="Slide 4">	-->
                </div>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>
