> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

lis4368

## Samuel Meador

### Project 1 Requirements:

 1. Modify Meta Tags
 2. Change title, navigation links, and header tags appropriately
 3. Change Carousel Pictures
 4. Working Client-Side Validation using Regular Expressions

#### README.md file should include the following items:

* Successful Validation
* Unsuccessful Validation 

#### Assignment Screenshots and Links:

![Successful Validation](img/success.png "Successful Validation")

![Unsuccessful Validation](img/unsuccess.png "Unsuccessful Validation")