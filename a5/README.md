> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

lis4368

## Samuel Meador

### Assignment 5 Requirements:

1. Compile Class and Servlet Files
2. Include server-side validation from A4
3. Display screenshots of pre-, post-valid user entry form entry, as well as MySQL customer table

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Associated Database Entry

#### Assignment Screenshots and Links:

*Screenshot of Valid User Form Entry*:

![A5 Valid User Form Entry](img/cust_submission_form.png "Valid User Form Entry")

*Screenshot of Passed Validation*:

![A5 Passed Validation](img/good_submission.png "Passed Validation")

*Screenshot of Associated Database Entry*

![A5 Associated Database Entry](img/mysql_results.png "Associated Database Entry")