> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

lis4368

## Samuel Meador

### Assignment 2 Requirements:

*Working Links*

1. [Link to Hello](http://localhost:9999/hello)
2. [Link to HelloHome](http://localhost:9999/hello/HelloHome.html)
3. [Link to sayhello](http://localhost:9999/hello/sayhello)
4. [Link to querybook](http://localhost:9999/hello/querybook.html)
5. [Link to sayhi](http://localhost:9999/hello/sayhi)

#### README.md file should include the following items:

* Screenshot of query results

#### Assignment Screenshots:

* Screenshot of Query Results http://localhost:9999/hello/querybook.html:

![Query Results Screenshot](img/queryresults.PNG)